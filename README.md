# Online Course Price Tracker #
**This project is hosted live on:** [https://course-price-tracker.herokuapp.com/](https://course-price-tracker.herokuapp.com/)    
  
**Source Code:** [https://bitbucket.org/mdsohail2022/course-price-tracker/src/master/](https://bitbucket.org/mdsohail2022/course-price-tracker/src/master/)

---
**About:**

A program which checks the price of an online course in a periodic interval and alerts the user through a WhatsApp message when the current price falls below the desired price. Implemented CI/CD pipeline to automate the entire development process from build to deployment.

**Software used:** 

Python, Twilio API, CI/CD tools - Git, BitBucket, CircleCI, Docker, AWS ECR, Heroku PaaS

---
**CI/CD pipeline:**

![CI CD pipeline](https://i.ibb.co/Px2n161/screenshot22.png "CI/CD pipeline")

---
**Project Demo:**

[![project demo](https://i.ibb.co/Wg6fC7Y/embed.png)](https://youtu.be/z8QcqVU2Kew)
