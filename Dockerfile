FROM python:3.8-alpine3.11

WORKDIR /app

COPY requirements.txt .

RUN pip install gunicorn
RUN pip install -r requirements.txt

COPY . .

CMD ["gunicorn", "-b", "0.0.0.0:8000", "app:app", "--workers=5"]
#CMD ["python","app.py"]


