import config

from datetime import datetime
import requests

from flask import Flask, render_template

from twilio.rest import Client

account_sid = config.twilio_account_sid
auth_token = config.twilio_auth_token
client = Client(account_sid, auth_token)

URL = 'https://www.udemy.com/course/learn-flutter-dart-to-build-ios-android-apps/'

course_id = 1708340

course = requests.get("https://www.udemy.com/api-2.0/courses/"+str(course_id)+"?fields[course]=discount,title,headline")

course_json  = course.json()

desired_price = 22


app = Flask(__name__)


def scrape_price():
    course_title = course_json['title']
    current_price = course_json['discount']['price']['amount']
    currency = course_json['discount']['price']['currency']
    return [course_title,current_price,currency]

list = scrape_price()

course_title = (list[0])
current_price = (list[1])
currency = (list[2])

def get_current_date():
    now = datetime.now()
    dt_string = now.strftime("%B %d, %Y")
    return dt_string


def get_current_time():
    now = datetime.now()
    tm_string = now.strftime("%H:%M:%S")
    return tm_string


def check_price():
    if(current_price <= desired_price):
        return True 
    else:
        return False


alert_body = f"Course Title: {course_title}...\nCurrent Price: {current_price} {currency}...\nYour desired price: {desired_price} {currency}...\nLink: {URL}"

def send_whatsapp_msg():
    message = client.messages.create(
                                  body = alert_body,
                                  from_ = config.whatsapp_from,
                                  to = config.whatsapp_to
                              )
    print("Whatsapp msg sent successfully")
    
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')

#generate html & checks the price after desired interval of time
def generate_html(t,d,r):
    html = """
        <html>
        <head>
            <meta http-equiv="refresh" content="30">
        </head>
        <body>
            <div style='text-align:center;font-size:55px;'>
                {0}
                <br>{1}
                <br>{2}
                <br>Current Price: {3} {4}
                <br>Desired Price: {5} {6}
		<br>Is current price lesser: {7}
                <br>Checking again after some time
            </div>
        </body>
        </html>""".format(t,d,course_title,current_price,currency,desired_price,currency,r)
    return html

def generate_html_success(t,d,r):
    html = """
        <html>
        <head>
        </head>
        <body>
            <div style='text-align:center;font-size:55px;'>
                {0}
                <br>{1}
                <br>{2}
                <br>Current Price: {3} {4}
                <br>Desired Price: {5} {6}
		<br>Is current price lesser: {7}
                <br>Whatsapp msg sent successfully</p>
            </div>
        </body>
        </html>""".format(t,d,course_title,current_price,currency,desired_price,currency,r)
    return html


@app.route('/')
def hello_world():
    time = get_current_time()
    date = get_current_date()
    is_price_less = check_price()
    if(is_price_less == True):
        send_whatsapp_msg()
        html = generate_html_success(time,date,is_price_less)
    else:
        html = generate_html(time,date,is_price_less)
    return html




